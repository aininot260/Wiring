//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 Wiring.rc 使用
//
#define IDD_WIRING_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDD_BAT_DIALOG                  132
#define IDC_VERTICE                     1001
#define IDC_EDGE                        1003
#define IDC_MAKEMAP                     1004
#define IDC_POINT1                      1006
#define IDC_POINT2                      1007
#define IDC_ADDEDGE                     1008
#define IDC_WORK                        1010
#define IDC_BAT                         1012
#define IDC_PIC                         1017
#define IDC_VALUE                       1018
#define IDC_TITLE                       1020
#define IDC_STEP1                       1021
#define IDC_STEP3                       1022
#define IDC_STEP4                       1023
#define IDC_RESET                       1024
#define IDC_BANK                        1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
