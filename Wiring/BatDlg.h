#pragma once


// BatDlg 对话框

class BatDlg : public CDialogEx
{
	DECLARE_DYNAMIC(BatDlg)

public:
	BatDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~BatDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BAT_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	CFont font2;
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
