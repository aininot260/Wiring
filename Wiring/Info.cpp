#include "stdafx.h"
#include "Info.h"
int n, m;  //节点数和边数 
int tot;  //建图所用的辅助临时变量
G g[maxn];
int arrayWGraph::numberOfVertices() const
{
	return n;
}
int arrayWGraph::numberOfEdges() const
{
	return m;
}
bool arrayWGraph::existsEdge(int a, int b) const  //判断边是否存在
{
	for (int tmp = 1; tmp <= g[a].cur; tmp++)
	{
		if (g[a].e[tmp].t == b)
		{
			return true;
		}
	}
	return false;
}
void arrayWGraph::insertEdge(int a, int b, int c)  //加入一条边
{
	tot++;
	point tmp;
	tmp.t = b;
	tmp.w = c;
	g[a].cur++;
	int t = g[a].cur;
	g[a].e[t] = tmp;
}
void arrayWGraph::eraseEdge(int a, int b)  //删除一条边
{
	for (int tmp = 1; tmp <= g[a].cur; tmp++)
	{
		if (g[a].e[tmp].t == b)
		{
			g[a].e[tmp].vis = 1;
			break;
		}
	}
}
int arrayWGraph::degree(int x) const  //顶点的度数
{
	return g[x].cur;
}
bool arrayWGraph::directed() const  //无向图
{
	return false;
}
bool arrayWGraph::weighted() const  //加权图
{
	return true;
}
void arrayWGraph::iterator(int x) const  //每个点所引出的每一条边
{
	for (int tmp = 1; tmp <= g[x].cur; tmp++)
	{
		cout << g[x].e[tmp].t << " ";
	}
	cout << endl;
}
bool flag;  //判断环路的辅助临时变量 
int vis[maxn];  //判断环路的辅助临时变量
void dfs(int x, int fa)
{
	for (int tmp = 1; tmp <= g[x].cur; tmp++)
	{
		if (g[x].e[tmp].t == fa)
			continue;
		if (vis[g[x].e[tmp].t])
		{
			flag = true;
		}
		if (!g[x].e[tmp].vis && !vis[g[x].e[tmp].t])  //如果该边存在并且to节点没有被访问过 
		{
			vis[g[x].e[tmp].t] = 1;
			cout << g[x].e[tmp].t << " ";
			dfs(g[x].e[tmp].t, x);
		}
	}
}
bool arrayWGraph::cycle() const
{
	flag = false;
	memset(vis, 0, sizeof(vis));
	vis[1] = 1;
	cout << 1 << " ";
	dfs(1, 0);  //从第一个节点开始进行深度优先遍历 
	cout << endl;
	return flag;
}
int fa[maxn];  //记录每一个节点的前驱节点 
int dis[maxn];  //记录每一个节点到当前节点的距离 
int tot1;  //记录需要添加的边数 
int b1[maxn];  //记录每一条边的较小顶点 
int b2[maxn];  //记录每一条边的较大顶点 
int ans;  //记录总的权值之和 
void arrayWGraph::analyse()
{
	memset(vis, 0, sizeof(vis));
	int cnt = 0;
	int p = 1;  //选择第一个节点作为起始节点
	for (int i = 1; i <= n; i++)
		fa[i] = p;  //规定每一个节点的前驱节点都为p 
	vis[p] = 1;  //标记上第一个节点已经访问过了 
	cnt++;  //节点计数增加 
	for (int i = 1; i <= n; i++)
	{
		for (int tmp = 1; tmp <= g[p].cur; tmp++)  //初始化每一个节点到当前的p节点的距离为图上的初始距离 
		{
			if (g[p].e[tmp].t == i)
				dis[i] = g[p].e[tmp].w;
		}
		if (dis[i] == 0)
			dis[i] = INF;
	}
	while (cnt != n)
	{
		int l = INF;
		int q;  //该节点就是p节点所连得最小节点 
		for (int i = 1; i <= n; i++)
		{
			if (l>dis[i] && !vis[i])  //找到没有访问过的节点中距离当前节点最小的那个节点 
			{
				l = dis[i];  //更新当前的最小距离 
				q = i;  //记录当前的临界最小节点 
			}
		}
		vis[q] = true;  //标记当前节点已经访问过了 
		p = q;  //将找到的那个p节点作为初始节点 
		if (l != INF)  //fa[p]就是这条边的起始节点，p就是当前节点 
		{
			tot1++;
			b1[tot1] = min(p, fa[p]);
			b2[tot1] = max(p, fa[p]);
		}
		cnt++;  //让当前节点数自增1 
		ans += l;  //最终结果加上当前得那个节点 
		for (int i = 1; i <= n; i++)
			if (!vis[i])
			{
				int t = INF;
				for (int tmp = 1; tmp <= g[p].cur; tmp++)  //初始化每一个节点到当前的p节点的距离为图上的初始距离 
				{
					if (g[p].e[tmp].t == i)
						t = g[p].e[tmp].w;
				}
				if (dis[i]>t)
				{
					dis[i] = t;
					fa[i] = p;
				}
			}
	}
}
string arrayWGraph::output()
{
	if (ans < 0)
		return "所给的数据不满足生成树的条件！\r\n";
	string res="";
	res=res+ "该生成树所需添加的边数为:";
	stringstream ss;
	string stot1;
	ss << tot1;
	ss >> stot1;
	ss.str("");
	res=res+ stot1 +"\r\n";
	res = res + "针对每一条边，它的两个邻接节点为:" + "\r\n";
	for (int i = 1; i <= tot1; i++)
	{
		string si,sb1,sb2;
		ss.clear();
		ss << i;
		ss >> si;
		ss.str("");
		ss.clear();
		ss << b1[i];
		ss >> sb1;
		ss.str("");
		ss.clear();
		ss << b2[i];
		ss >> sb2;
		ss.str("");
		res=res+ si + ":" + sb1 + " " + sb2 + "\r\n";
	}

	res=res+ "该生成树的总长度为:";
	string sans;
	ss.clear();
	ss << ans;
	ss >> sans;
	ss.str("");
	res=res+ sans + "\r\n";
	return res;
}