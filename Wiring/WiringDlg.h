
// WiringDlg.h : 头文件
//

#pragma once


// CWiringDlg 对话框
class CWiringDlg : public CDialogEx
{
// 构造
public:
	CWiringDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_WIRING_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;
	CFont font;
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedMakemap();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedAddedge();
	afx_msg void OnBnClickedWork();
	afx_msg void OnBnClickedReset();
	afx_msg void OnBnClickedBat();
};
