
// WiringDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Wiring.h"
#include "WiringDlg.h"
#include "afxdialogex.h"
#include"Info.h"
#include"BatDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CWiringDlg 对话框



CWiringDlg::CWiringDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_WIRING_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWiringDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	font.CreatePointFont(300, L"楷体");
	GetDlgItem(IDC_TITLE)->SetFont(&font);
}

BEGIN_MESSAGE_MAP(CWiringDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_MAKEMAP, &CWiringDlg::OnBnClickedMakemap)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_ADDEDGE, &CWiringDlg::OnBnClickedAddedge)
	ON_BN_CLICKED(IDC_WORK, &CWiringDlg::OnBnClickedWork)
	ON_BN_CLICKED(IDC_RESET, &CWiringDlg::OnBnClickedReset)
	ON_BN_CLICKED(IDC_BAT, &CWiringDlg::OnBnClickedBat)
END_MESSAGE_MAP()


// CWiringDlg 消息处理程序
BOOL CWiringDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	GetDlgItem(IDC_POINT1)->EnableWindow(FALSE);
	GetDlgItem(IDC_POINT2)->EnableWindow(FALSE);
	GetDlgItem(IDC_VALUE)->EnableWindow(FALSE);
	GetDlgItem(IDC_ADDEDGE)->EnableWindow(FALSE);
	GetDlgItem(IDC_WORK)->EnableWindow(FALSE);
	
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}
arrayWGraph Graph;
// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。
int width, height;
int px[1005], py[1005];
int l[1005], r[1005],w[1005];
int cnt = 0;  //点的计数
int cnt2 = 0;  //边的计数
bool flag1 = false;
bool flag2 = false;
bool flag3 = false;
CRect rect1;
void CWiringDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		CWnd *pWin = GetDlgItem(IDC_PIC);//获取该控件的指针，就可以对该控件直接操作了
		pWin->GetClientRect(rect1);//把控件的长宽、坐标等信息保存在rect里
		width = rect1.Width();//可以获取宽和高
		height = rect1.Height();
		CDC *pDc = pWin->GetDC();//获取该控件的画布
								 //有了画布，下面可以自由的画图了，想怎么画就怎么话
		if (flag3)
		{
			pDc->Rectangle(rect1);
			CBrush myBrush;
			myBrush.CreateSolidBrush(RGB(255, 255, 255));
			pDc->FillRect(rect1, &myBrush);
			CPen pen1(PS_DOT, 3, RGB(0, 0, 0));//创建一个虚线线条，宽度为1，红色的画笔对象  
			CPen* pOldPen1 = pDc->SelectObject(&pen1);//将画笔对象选入到设备描述表中
			for (int i = 1; i <= cnt2; i++)
			{
				pDc->MoveTo(px[l[i]], py[l[i]]);
				pDc->LineTo(px[r[i]], py[r[i]]);
			}
			pDc->SelectObject(pOldPen1);//恢复设备描述表 
			pen1.DeleteObject();
			CPen pen2(PS_DOT, 3, RGB(255, 255, 255));//创建一个虚线线条，宽度为1，红色的画笔对象  
			CPen* pOldPen2 = pDc->SelectObject(&pen2);//将画笔对象选入到设备描述表中
			for (int i = 1; i <= tot1; i++)
			{
				pDc->MoveTo(px[b1[i]], py[b1[i]]);
				pDc->LineTo(px[b2[i]], py[b2[i]]);
			}
			pDc->SelectObject(pOldPen2);//恢复设备描述表 
			pen2.DeleteObject();
			CPen pen3(PS_DOT, 3, RGB(255, 0, 0));//创建一个虚线线条，宽度为1，红色的画笔对象  
			CPen* pOldPen3 = pDc->SelectObject(&pen3);//将画笔对象选入到设备描述表中
			for (int i = 1; i <= tot1; i++)
			{
				pDc->MoveTo(px[b1[i]], py[b1[i]]);
				pDc->LineTo(px[b2[i]], py[b2[i]]);
			}
			for (int i = 1; i <= cnt2; i++)
			{
				CString tmp;
				tmp.Format(_T("%d"), w[i]);
				CFont font;
				font.CreatePointFont(100, _T("黑体"));
				//载入字体
				CFont* pOldFont;
				pOldFont = pDc->SelectObject(&font);
				pDc->TextOutW((px[l[i]] + px[r[i]]) / 2, (py[l[i]] + py[r[i]]) / 2, tmp);
			}
			pDc->SelectObject(pOldPen3);//恢复设备描述表 
			pen3.DeleteObject();
			for (int i = 1; i <= cnt; i++)
			{
				CPen pen4(PS_DOT, 2, RGB(0, 0, 0));//创建一个虚线线条，宽度为1，红色的画笔对象  
				CPen* pOldPen4 = pDc->SelectObject(&pen4);//将画笔对象选入到设备描述表中
				pDc->Ellipse(px[i] - 20, py[i] - 20, px[i] + 20, py[i] + 20);
				pDc->SelectObject(pOldPen4);//恢复设备描述表 
				pen4.DeleteObject();
				CString tmp;
				tmp.Format(_T("%d"), i);
				CFont font;
				font.CreatePointFont(150, _T("黑体"));
				//载入字体
				CFont* pOldFont;
				pOldFont = pDc->SelectObject(&font);
				pDc->TextOutW(px[i] - 10, py[i] - 10, tmp);
			}
		}
		if (flag2)
		{
			pDc->Rectangle(rect1);
			CBrush myBrush;
			myBrush.CreateSolidBrush(RGB(255, 255, 255));
			pDc->FillRect(rect1, &myBrush);
			CPen pen1(PS_DOT, 3, RGB(0, 0, 0));//创建一个虚线线条，宽度为1，红色的画笔对象  
			CPen* pOldPen1 = pDc->SelectObject(&pen1);//将画笔对象选入到设备描述表中
			for (int i = 1; i <= cnt2; i++)
			{
				pDc->MoveTo(px[l[i]],py[l[i]]);
				pDc->LineTo(px[r[i]],py[r[i]]);
				CString tmp;
				tmp.Format(_T("%d"), w[i]);
				CFont font;
				font.CreatePointFont(100, _T("黑体"));
				//载入字体
				CFont* pOldFont;
				pOldFont = pDc->SelectObject(&font);
				pDc->TextOutW((px[l[i]]+ px[r[i]])/2, (py[l[i]] + py[r[i]]) / 2, tmp);
			}
			pDc->SelectObject(pOldPen1);//恢复设备描述表 
			pen1.DeleteObject();
			for (int i = 1; i <= cnt; i++)
			{
				CPen pen4(PS_DOT, 2, RGB(0, 0, 0));//创建一个虚线线条，宽度为1，红色的画笔对象  
				CPen* pOldPen4 = pDc->SelectObject(&pen4);//将画笔对象选入到设备描述表中
				pDc->Ellipse(px[i] - 20, py[i] - 20, px[i] + 20, py[i] + 20);
				pDc->SelectObject(pOldPen4);//恢复设备描述表 
				pen4.DeleteObject();
				CString tmp;
				tmp.Format(_T("%d"), i);
				CFont font;
				font.CreatePointFont(150, _T("黑体"));
				//载入字体
				CFont* pOldFont;
				pOldFont = pDc->SelectObject(&font);
				pDc->TextOutW(px[i] - 10, py[i] - 10, tmp);
			}
		}
		if (flag1)
		{
			pDc->Rectangle(rect1);
			CBrush myBrush;
			myBrush.CreateSolidBrush(RGB(255, 255, 255));
			pDc->FillRect(rect1, &myBrush);
			for (int i = 1; i <= cnt; i++)
			{
				CPen pen4(PS_DOT, 2, RGB(0, 0, 0));//创建一个虚线线条，宽度为1，红色的画笔对象  
				CPen* pOldPen4 = pDc->SelectObject(&pen4);//将画笔对象选入到设备描述表中
				pDc->Ellipse(px[i] - 20, py[i] - 20, px[i] + 20, py[i] + 20);
				pDc->SelectObject(pOldPen4);//恢复设备描述表 
				pen4.DeleteObject();
				CString tmp;
				tmp.Format(_T("%d"), i);
				CFont font;
				font.CreatePointFont(150, _T("黑体"));
				//载入字体
				CFont* pOldFont;
				pOldFont = pDc->SelectObject(&font);
				pDc->TextOutW(px[i]-10, py[i]-10, tmp);
			}
			if (cnt == n)
			{
				flag1 = false;
				GetDlgItem(IDC_POINT1)->EnableWindow(TRUE);
				GetDlgItem(IDC_POINT2)->EnableWindow(TRUE);
				GetDlgItem(IDC_VALUE)->EnableWindow(TRUE);
				GetDlgItem(IDC_ADDEDGE)->EnableWindow(TRUE);
			}
		}
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CWiringDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CWiringDlg::OnBnClickedMakemap()
{
	int f = 0;
	// TODO: 在此添加控件通知处理程序代码
	CString cstr;
	GetDlgItem(IDC_VERTICE)->GetWindowText(cstr);
	string str = (CStringA)cstr;
	if (str == "")
		f = 1;
	n = atoi(str.c_str());
	GetDlgItem(IDC_EDGE)->GetWindowText(cstr);
	str = (CStringA)cstr;
	if (str == "")
		f = 1;
	m = atoi(str.c_str());
	if (m < n - 1||n<2||m<1||m>((n-1)*n/2))  //对所输入的数据进行一个范围的界定
		f = 2;
	if (f==0)
	{
		flag1 = true;
		flag3 = false;
		Invalidate();
		GetDlgItem(IDC_VERTICE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDGE)->EnableWindow(FALSE);
		GetDlgItem(IDC_MAKEMAP)->EnableWindow(FALSE);
	}
	if(f==2)
		AfxMessageBox(_T("输入的建图信息不符合规范！"), MB_OK | MB_ICONERROR);
	if(f==1)
		AfxMessageBox(_T("请输入完整的建图信息！"), MB_OK | MB_ICONERROR);
	
}

void CWiringDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (flag1)
	{
		if (point.x > rect1.left && point.x < rect1.right+100 && point.y>rect1.top && point.y < rect1.bottom+100)
		{  //对光标的区域进行一个相应的界定
			cnt++;
			px[cnt] = point.x-32;
			py[cnt] = point.y-107;
			Invalidate();
		}
	}
}


void CWiringDlg::OnBnClickedAddedge()
{
	if (cnt2 != m)
	{
		int f = 0;
		flag2 = true;
		int x0, y0, z0;
		CString cstr;
		GetDlgItem(IDC_POINT1)->GetWindowText(cstr);
		string str = (CStringA)cstr;
		if (str == "")
			f = 1;
		x0 = atoi(str.c_str());
		GetDlgItem(IDC_POINT2)->GetWindowText(cstr);
		str = (CStringA)cstr;
		if (str == "")
			f = 1;
		y0 = atoi(str.c_str());
		GetDlgItem(IDC_VALUE)->GetWindowText(cstr);
		str = (CStringA)cstr;
		if (str == "")
			f = 1;
		z0 = atoi(str.c_str());
		for (int i = 1; i <= cnt2; i++)
		{
			if ((l[i] == x0&&r[i] == y0) || (l[i] == y0&&r[i] == x0))
				f = 2;
		}
		if (x0 == y0 || z0 == 0||x0<1||x0>n||y0<1||y0>n)  //在MFC中调用相应的建图函数
			f = 3;
		if (f == 0)
		{
			Graph.insertEdge(x0, y0, z0);
			Graph.insertEdge(y0, x0, z0);
			cnt2++;
			l[cnt2] = x0;
			r[cnt2] = y0;
			w[cnt2] = z0;
			Invalidate();
			GetDlgItem(IDC_POINT1)->SetWindowText(_T(""));
			GetDlgItem(IDC_POINT2)->SetWindowText(_T(""));
			GetDlgItem(IDC_VALUE)->SetWindowText(_T(""));
		}
		if(f==1)
			AfxMessageBox(_T("请输入完整的建图内容！"), MB_OK | MB_ICONERROR);
		if(f==2)
			AfxMessageBox(_T("该边已经添加过了，请勿重复添加。"), MB_OK | MB_ICONERROR);
		if (f == 3)
			AfxMessageBox(_T("输入的信息有误。"), MB_OK | MB_ICONERROR);
		if (cnt2 == m)
		{
			GetDlgItem(IDC_POINT1)->EnableWindow(FALSE);
			GetDlgItem(IDC_POINT2)->EnableWindow(FALSE);
			GetDlgItem(IDC_VALUE)->EnableWindow(FALSE);
			GetDlgItem(IDC_ADDEDGE)->EnableWindow(FALSE);
			GetDlgItem(IDC_WORK)->EnableWindow(TRUE);
		}
	}
}

bool flag4 = false;
void CWiringDlg::OnBnClickedWork()
{
	// TODO: 在此添加控件通知处理程序代码
	if(!flag4)
	Graph.analyse();
	flag2 = false;
	flag3 = true;
	Invalidate();
	string tmp = Graph.output();
	CString ctmp(tmp.c_str());
	flag4 = true;
	AfxMessageBox(ctmp, MB_OK| MB_ICONINFORMATION);
}


void CWiringDlg::OnBnClickedReset()
{
	// TODO: 在此添加控件通知处理程序代码
	flag1 = flag2 = flag3 = false;
	flag4 = false;
	cnt = cnt2 = 0;
	memset(px, 0, sizeof(px));
	memset(py, 0, sizeof(py));
	memset(l, 0, sizeof(l));
	memset(r, 0, sizeof(r));
	n = m = 0;
	ans = 0;
	memset(fa, 0, sizeof(fa));
	memset(dis, 0, sizeof(dis));
	tot = 0;
	memset(g, 0, sizeof(g));
	tot1 = 0;
	memset(b1, 0, sizeof(b1));
	memset(b2, 0, sizeof(b2));
	flag=0;  //判断环路的辅助临时变量 
	GetDlgItem(IDC_VERTICE)->EnableWindow(TRUE);
	GetDlgItem(IDC_EDGE)->EnableWindow(TRUE);
	GetDlgItem(IDC_MAKEMAP)->EnableWindow(TRUE);
	GetDlgItem(IDC_WORK)->EnableWindow(FALSE);
	GetDlgItem(IDC_POINT1)->EnableWindow(FALSE);
	GetDlgItem(IDC_POINT2)->EnableWindow(FALSE);
	GetDlgItem(IDC_VALUE)->EnableWindow(FALSE);
	GetDlgItem(IDC_ADDEDGE)->EnableWindow(FALSE);
}


void CWiringDlg::OnBnClickedBat()
{
	// TODO: 在此添加控件通知处理程序代码
	n = m = 0;
	ans = 0;
	memset(fa, 0, sizeof(fa));
	memset(dis, 0, sizeof(dis));
	tot = 0;
	memset(g, 0, sizeof(g));
	tot1 = 0;
	memset(b1, 0, sizeof(b1));
	memset(b2, 0, sizeof(b2));
	flag = 0;  //判断环路的辅助临时变量 
	GetDlgItem(IDC_WORK)->EnableWindow(FALSE);
	BatDlg batdlg;
	batdlg.DoModal();
}
