// BatDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Wiring.h"
#include "BatDlg.h"
#include "afxdialogex.h"
#include"Info.h"

// BatDlg 对话框

IMPLEMENT_DYNAMIC(BatDlg, CDialogEx)

BatDlg::BatDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_BAT_DIALOG, pParent)
{

}

BatDlg::~BatDlg()
{
}

void BatDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	font2.CreatePointFont(150, L"黑体");
	GetDlgItem(IDC_BANK)->SetFont(&font2);
}


BEGIN_MESSAGE_MAP(BatDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &BatDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// BatDlg 消息处理程序


void BatDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	ifstream fin("input.txt");  //从文件中读取数据
	arrayWGraph Graph;  //声明一个图对象
	while (!fin.eof())
	{
		fin >> n >> m;
		int x0, y0, z0;
		for (int i = 1; i <= m; i++)
		{
			fin >> x0 >> y0 >> z0;
			Graph.insertEdge(x0, y0, z0);  //无向图双向建边
			Graph.insertEdge(y0, x0, z0);
		}
	}
	fin.close();
	Graph.analyse();
	string tmp = Graph.output();
	CString cstr(tmp.c_str());
	GetDlgItem(IDC_BANK)->SetWindowText(cstr);
	n = m = 0;
	ans = 0;
	memset(fa, 0, sizeof(fa));
	memset(dis, 0, sizeof(dis));
	tot = 0;
	memset(g, 0, sizeof(g));
	tot1 = 0;
	memset(b1, 0, sizeof(b1));
	memset(b2, 0, sizeof(b2));
	flag = 0;  //判断环路的辅助临时变量 
}
