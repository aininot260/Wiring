#pragma once
#include<string>
using namespace std;
#define maxn 1005 
#define maxm 1005 
const int INF = 0x7fffffff;
struct point  //每一个节点
{
	bool vis;  //标记是否被删除 
	int t, w;  //to节点，权值
};
struct G  //邻接数组
{
	int cur;  //记录当前数组中元素个数 
	point e[maxm];  //每一个节点所引出的数组 
};
extern int n, m;  //节点数和边数 
extern int tot;  //建图所用的辅助临时变量
extern G g[maxn];
extern int fa[maxn];  //记录每一个节点的前驱节点 
extern int dis[maxn];  //记录每一个节点到当前节点的距离 
extern int tot1;  //记录需要添加的边数 
extern int b1[maxn];  //记录每一条边的较小顶点 
extern int b2[maxn];  //记录每一条边的较大顶点 
extern bool flag;  //判断环路的辅助临时变量 
extern int vis[maxn];  //判断环路的辅助临时变量
extern int ans;  //记录总的权值之和 
class graph
{
public:
	virtual ~graph() {}

	virtual int numberOfVertices() const = 0;  //返回顶点的数目 
	virtual int numberOfEdges() const = 0;  //返回边的数目 
	virtual bool existsEdge(int, int) const = 0;  //判断边是否存在 
	virtual void insertEdge(int, int, int) = 0;  //插入一条新的边 
	virtual void eraseEdge(int, int) = 0;  //将指定的边删除 
	virtual int degree(int) const = 0;   //返回定点的度数 
	virtual bool directed() const = 0;  //判断是否是无向图 
	virtual bool weighted() const = 0;  //判断是否是加权图 
	virtual void iterator(int) const = 0;  //访问指定顶点的相邻顶点 
	virtual bool cycle() const = 0;  //判断一个无向图是否有环路
};
class arrayWGraph : public graph
{
public:
	int numberOfVertices() const;
	int numberOfEdges() const;
	bool existsEdge(int, int) const;
	void insertEdge(int, int, int);
	void eraseEdge(int, int);
	int degree(int) const;
	bool directed() const;
	bool weighted() const;
	void iterator(int) const;
	bool cycle() const;
	void analyse();  //核心算法，计算出权值之和并记录生成树添加的每一条边的相应顶点 
	string output();
};

